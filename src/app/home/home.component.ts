import { Component, OnInit } from '@angular/core';
import { AppService } from '../app.service';
import { IImage } from '../models/image';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  images:IImage[];
  sortOptions =[
    {name:'Oldest First', value:'timeDsc'},
    {name:'Recent First', value:'timeAsc'},
    {name:'Most Likes', value:'likeDsc'},
    {name:'Least Likes', value:'likeAsc'},
  ];

  constructor(private appService:AppService) { }

  ngOnInit(): void {
    this.getAllImages('timeDsc');
  }
  getAllImages(sort:string) {
    this.appService.getAllImages().subscribe(images =>{
      if(images){
        switch (sort){
          case 'timeAsc':
            this.images = images.sort((a,b)=>{ return a.timestamp.localeCompare(b.timestamp)}).reverse();
            break;
          case 'timeDsc':
            this.images = images.sort((a,b)=>{ return a.timestamp.localeCompare(b.timestamp)});
            break;
          case 'likeDsc':
            this.images = images.sort((a,b)=>{ return a.likes -b.likes}).reverse();
            break;
          case 'likeAsc':
            this.images = images.sort((a,b)=>{ return a.likes -b.likes});
            break;
        }
        
      }
    }, error =>{console.log(error);})
  }

  onSortSelected(sort:string){
    this.getAllImages(sort);
  }
  

}
