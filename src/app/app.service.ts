import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IImage } from './models/image';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  baseUrl = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getAllImages():Observable<IImage[]>{
    return this.http.get<IImage[]>(this.baseUrl);
  }
}
