import { Component, Input, OnInit } from '@angular/core';
import { IImage } from '../models/image';

@Component({
  selector: 'app-image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.css']
})
export class ImageCardComponent implements OnInit {
  @Input() image:IImage;

  constructor() { }

  ngOnInit(): void {
  }

}
