export interface IImage {
    Image: string;
    likes: number;
    timestamp: string;
}